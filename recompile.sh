#!/bin/bash
PKG="x11-wm/dwm"
read -p "Enter forked from commit: " FORKED_FROM_COMMIT
if [ -z ${FORKED_FROM_COMMIT} ]; then
    FORKED_FROM_COMMIT="forkedfrom"
fi
git format-patch ${FORKED_FROM_COMMIT}
sudo mkdir -pv /etc/portage/patches/${PKG}
sudo rm /etc/portage/patches/${PKG}/*.patch
sudo mv *.patch /etc/portage/patches/${PKG}/
sudo emerge -v --quiet-build ${PKG}
